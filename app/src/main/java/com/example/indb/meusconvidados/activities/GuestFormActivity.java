package com.example.indb.meusconvidados.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.indb.meusconvidados.R;
import com.example.indb.meusconvidados.business.GuestBusiness;
import com.example.indb.meusconvidados.constants.GuestConstants;
import com.example.indb.meusconvidados.models.Guest;

public class GuestFormActivity extends AppCompatActivity implements View.OnClickListener{

    private static class ViewHolder {

        EditText mEditName;
        EditText mEditDocument;
        RadioButton mRadioNotConfirmed;
        RadioButton mRadioPresent;
        RadioButton mRadioAbsent;
        Button mButtonSave;
    }
    private ViewHolder mViewHolder = new ViewHolder();
    private GuestBusiness mGuestBusiness;
    private int mGuestId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_form);

        this.mViewHolder.mEditName = this.findViewById(R.id.edit_name);
        this.mViewHolder.mEditDocument = this.findViewById(R.id.edit_document);
        this.mViewHolder.mRadioPresent = this.findViewById(R.id.radio_present);
        this.mViewHolder.mRadioNotConfirmed = this.findViewById(R.id.radio_not_confirmed);
        this.mViewHolder.mRadioAbsent = this.findViewById(R.id.radio_absent);
        this.mViewHolder.mButtonSave = this.findViewById(R.id.button_save);

        this.mViewHolder.mRadioNotConfirmed.setChecked(true);
        this.mGuestBusiness = new GuestBusiness(this);
        setOnClickListener();
        loadDataFromActivity();

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_save){
            handleSave();
        }
    }

    private void setOnClickListener(){
        this.mViewHolder.mRadioPresent.setOnClickListener(this);
        this.mViewHolder.mRadioNotConfirmed.setOnClickListener(this);
        this.mViewHolder.mRadioAbsent.setOnClickListener(this);
        this.mViewHolder.mButtonSave.setOnClickListener(this);
    }

    private void loadDataFromActivity() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            this.mGuestId = bundle.getInt(GuestConstants.BundleConstants.GUEST_ID);

            Guest guest = this.mGuestBusiness.load(this.mGuestId);

            this.mViewHolder.mEditName.setText(guest.getName());
            this.mViewHolder.mEditDocument.setText(guest.getDocument());
            if (guest.getConfirmed() == GuestConstants.CONFIRMATION.PRESENT){
                this.mViewHolder.mRadioPresent.setChecked(true);
            }else if(guest.getConfirmed() == GuestConstants.CONFIRMATION.ABSENT){
                this.mViewHolder.mRadioAbsent.setChecked(true);
            }else{
                this.mViewHolder.mRadioNotConfirmed.setChecked(true);
            }
        }
    }

    private void handleSave() {
        if (!this.validateSave()) {
            return;
        }
        Guest guest = new Guest();

        guest.setName(this.mViewHolder.mEditName.getText().toString());
        guest.setDocument(this.mViewHolder.mEditDocument.getText().toString());

        if (this.mViewHolder.mRadioAbsent.isChecked()) {
            guest.setConfirmed(GuestConstants.CONFIRMATION.ABSENT);
        } else if (this.mViewHolder.mRadioNotConfirmed.isChecked()) {
            guest.setConfirmed(GuestConstants.CONFIRMATION.NOT_CONFIRMED);
        } else {
            guest.setConfirmed(GuestConstants.CONFIRMATION.PRESENT);
        }

        if (this.mGuestId == 0) {
            if (this.mGuestBusiness.insert(guest)) {
                Toast.makeText(this, R.string.guest_saved, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show();
            }
            finish();
        }else{
            guest.setId(mGuestId);
            if (this.mGuestBusiness.update(guest)) {
                Toast.makeText(this, R.string.updated, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show();
            }
            finish();
        }

}

    private boolean validateSave() {
        if(this.mViewHolder.mEditName.getText().toString().isEmpty()){
            this.mViewHolder.mEditName.setError(getString(R.string.name_required));
            return false;
        }
        return true;
    }


}
