package com.example.indb.meusconvidados.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.indb.meusconvidados.constants.DatabaseConstants;
import com.example.indb.meusconvidados.constants.GuestConstants;
import com.example.indb.meusconvidados.models.Guest;
import com.example.indb.meusconvidados.models.GuestCount;

import java.util.ArrayList;
import java.util.List;


public class GuestRepository {

    private static GuestRepository INSTANCE;
    private GuestDatabaseHelper mGuestDatabaseHelper;

    private GuestRepository (Context context) {
        this.mGuestDatabaseHelper = new GuestDatabaseHelper(context);
    }

    public static synchronized GuestRepository getInstance(Context context) {
        if (INSTANCE == null){
            INSTANCE = new GuestRepository(context);
        }
        return INSTANCE;
    }

    public boolean insert(Guest guest){
        try {
            SQLiteDatabase sqLiteDatabase = this.mGuestDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseConstants.GUEST.COLUMNS.NAME, guest.getName());
            contentValues.put(DatabaseConstants.GUEST.COLUMNS.PRESENCE, guest.getConfirmed());
            contentValues.put(DatabaseConstants.GUEST.COLUMNS.DOCUMENT, guest.getDocument());
            sqLiteDatabase.insert(DatabaseConstants.GUEST.TABLE_NAME, null, contentValues);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public List<Guest> getGuestsByQuery(String query) {
        List<Guest> list = new ArrayList<>();

        try{
            SQLiteDatabase sqLiteDatabase = this.mGuestDatabaseHelper.getReadableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery(query, null);
            if (cursor!= null && cursor.getCount() > 0){
                while (cursor.moveToNext()){
                    Guest guest = new Guest();
                    guest.setId(cursor.getInt(cursor.getColumnIndex(DatabaseConstants.GUEST.COLUMNS.ID)));
                    guest.setName(cursor.getString(cursor.getColumnIndex(DatabaseConstants.GUEST.COLUMNS.NAME)));
                    guest.setConfirmed(cursor.getInt(cursor.getColumnIndex(DatabaseConstants.GUEST.COLUMNS.PRESENCE)));

                    list.add(guest);

                }
            }

            if(cursor != null){
                cursor.close();
            }
        }catch (Exception e){
            return list;
        }

        return list;
    }

    public Guest load(int mGuestId) {
        Guest guest = new Guest();

        try {
            SQLiteDatabase sqLiteDatabase = this.mGuestDatabaseHelper.getReadableDatabase();

            String[] projection = {
                    DatabaseConstants.GUEST.COLUMNS.ID,
                    DatabaseConstants.GUEST.COLUMNS.NAME,
                    DatabaseConstants.GUEST.COLUMNS.PRESENCE,
                    DatabaseConstants.GUEST.COLUMNS.DOCUMENT
            };

            String selection = DatabaseConstants.GUEST.COLUMNS.ID + " = ?";
            String[] selectionArgs ={String.valueOf(mGuestId)};

            Cursor cursor = sqLiteDatabase.query(DatabaseConstants.GUEST.TABLE_NAME, projection,
                    selection, selectionArgs, null, null, null);

            if (cursor != null && cursor.getCount() > 0){
                cursor.moveToFirst();
                guest.setId(cursor.getInt(cursor.getColumnIndex(DatabaseConstants.GUEST.COLUMNS.ID)));
                guest.setName(cursor.getString(cursor.getColumnIndex(DatabaseConstants.GUEST.COLUMNS.NAME)));
                guest.setConfirmed(cursor.getInt(cursor.getColumnIndex(DatabaseConstants.GUEST.COLUMNS.PRESENCE)));
                guest.setDocument(cursor.getString(cursor.getColumnIndex(DatabaseConstants.GUEST.COLUMNS.DOCUMENT)));
            }

            if (cursor != null){
                cursor.close();
            }
            return guest;

        }catch (Exception e){
            return guest;
        }

    }

    public boolean update(Guest guest) {

        try{
            SQLiteDatabase sqLiteDatabase = this.mGuestDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseConstants.GUEST.COLUMNS.NAME, guest.getName());
            contentValues.put(DatabaseConstants.GUEST.COLUMNS.PRESENCE, guest.getConfirmed());
            contentValues.put(DatabaseConstants.GUEST.COLUMNS.DOCUMENT, guest.getDocument());

            String selection = DatabaseConstants.GUEST.COLUMNS.ID + " = ?";
            String[] selectionArgs = {String.valueOf(guest.getId())};

            sqLiteDatabase.update(DatabaseConstants.GUEST.TABLE_NAME, contentValues, selection, selectionArgs);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean remove(int id) {

       try {
           SQLiteDatabase sqLiteDatabase = this.mGuestDatabaseHelper.getWritableDatabase();

           String whereClause = DatabaseConstants.GUEST.COLUMNS.ID + " = ?";
           String[] whereArgs = {String.valueOf(id)};

           sqLiteDatabase.delete(DatabaseConstants.GUEST.TABLE_NAME, whereClause, whereArgs);
           return true;
       }catch (Exception e){
           return false;
       }
    }

    public GuestCount loadDashboard() {
        GuestCount guestCount = new GuestCount(0,0,0);
        Cursor cursor;

        try{
            SQLiteDatabase sqLiteDatabase = this.mGuestDatabaseHelper.getReadableDatabase();

            cursor = sqLiteDatabase.rawQuery(
                    "select count (*) from " +
                            DatabaseConstants.GUEST.TABLE_NAME +
                            " where " +
                            DatabaseConstants.GUEST.COLUMNS.PRESENCE +
                            " = " +
                            GuestConstants.CONFIRMATION.PRESENT,
                    null);

            if (cursor != null &&  cursor.getCount() > 0){
                cursor.moveToFirst();
                guestCount.setPresentCount(cursor.getInt(0));
            }


            cursor = sqLiteDatabase.rawQuery(
                    "select count (*) from " +
                            DatabaseConstants.GUEST.TABLE_NAME +
                            " where " +
                            DatabaseConstants.GUEST.COLUMNS.PRESENCE +
                            " = " +
                            GuestConstants.CONFIRMATION.ABSENT,
                    null);

            if (cursor != null &&  cursor.getCount() > 0){
                cursor.moveToFirst();
                guestCount.setAbsentCount(cursor.getInt(0));
            }

            cursor = sqLiteDatabase.rawQuery(
                    "select count (*) from " + DatabaseConstants.GUEST.TABLE_NAME, null);

            if (cursor != null &&  cursor.getCount() > 0){
                cursor.moveToFirst();
                guestCount.setAllCount(cursor.getInt(0));
            }

            if (cursor != null){
                cursor.close();
            }

            return guestCount;

        }catch (Exception e){
            return guestCount;
        }
    }
}
