package com.example.indb.meusconvidados.listeners;

public interface OnGuestListInteractionListener {

    void onListClick(int id);

    void onDeleteClick(int id);
}
