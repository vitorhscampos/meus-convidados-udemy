package com.example.indb.meusconvidados.viewholder;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.indb.meusconvidados.R;
import com.example.indb.meusconvidados.models.Guest;
import com.example.indb.meusconvidados.listeners.OnGuestListInteractionListener;

public class GuestViewHolder extends RecyclerView.ViewHolder {

    private TextView mTextName;
    private Context mContext;

    public GuestViewHolder(View itemView, Context context) {
        super(itemView);

        this.mTextName = itemView.findViewById(R.id.text_name);
        this.mContext = context;
    }

    public void bindData(final Guest guest, final OnGuestListInteractionListener listener) {
        this.mTextName.setText(guest.getName());
        this.mTextName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onListClick(guest.getId());
            }
        });

        this.mTextName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle(R.string.remove_guest_dialog)
                        .setMessage(R.string.remove_guest)
                        .setIcon(R.drawable.remove)
                        .setPositiveButton(R.string.general_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                listener.onDeleteClick(guest.getId());
                            }
                        })
                        .setNeutralButton(R.string.general_no, null)
                        .show();
                return true;
            }
        });


    }
}
