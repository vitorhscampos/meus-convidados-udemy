package com.example.indb.meusconvidados.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.indb.meusconvidados.R;
import com.example.indb.meusconvidados.models.Guest;
import com.example.indb.meusconvidados.listeners.OnGuestListInteractionListener;
import com.example.indb.meusconvidados.viewholder.GuestViewHolder;

import java.util.List;

public class GuestListAdapter extends RecyclerView.Adapter<GuestViewHolder> {

    private List<Guest> mGuestList;
    private OnGuestListInteractionListener mOnGuestListInteractionListener;

    public GuestListAdapter(List<Guest> guestList, OnGuestListInteractionListener listener) {
        this.mGuestList = guestList;
        this.mOnGuestListInteractionListener = listener;
    }

    @Override
    public GuestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View guestView = layoutInflater.inflate(R.layout.row_guest_list, parent, false);

        return new GuestViewHolder(guestView, context);
    }

    @Override
    public void onBindViewHolder(GuestViewHolder holder, int position) {

        Guest guest = this.mGuestList.get(position);
        holder.bindData(guest, mOnGuestListInteractionListener);
    }

    @Override
    public int getItemCount() {
        return this.mGuestList.size();
    }
}
