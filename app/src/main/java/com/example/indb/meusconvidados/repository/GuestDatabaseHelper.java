package com.example.indb.meusconvidados.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.indb.meusconvidados.constants.DatabaseConstants;

public class GuestDatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "MeusConvidados.db";

    private static final String SQL_CREATE_TABLE_GUEST =
            "create table " + DatabaseConstants.GUEST.TABLE_NAME + " ("
                    + DatabaseConstants.GUEST.COLUMNS.ID + " integer primary key autoincrement, "
                    + DatabaseConstants.GUEST.COLUMNS.NAME + " text, "
                    + DatabaseConstants.GUEST.COLUMNS.DOCUMENT + " text, "
                    + DatabaseConstants.GUEST.COLUMNS.PRESENCE + " integer);";


    private static final String DROP_TABLE_GUEST = "DROP TABLE IF EXISTS " + DatabaseConstants.GUEST.TABLE_NAME;

    public GuestDatabaseHelper(Context context) {
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_GUEST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DROP_TABLE_GUEST);
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_GUEST);
    }
}
