package com.example.indb.meusconvidados.business;

import android.content.Context;

import com.example.indb.meusconvidados.constants.DatabaseConstants;
import com.example.indb.meusconvidados.constants.GuestConstants;
import com.example.indb.meusconvidados.models.Guest;
import com.example.indb.meusconvidados.models.GuestCount;
import com.example.indb.meusconvidados.repository.GuestRepository;

import java.util.List;

public class GuestBusiness {
    private GuestRepository mGuestRepository;

    public GuestBusiness(Context context){
        mGuestRepository = GuestRepository.getInstance(context);
    }

    public boolean insert (Guest guest){
       return this.mGuestRepository.insert(guest);
    }

    public List<Guest> getInvited() {
       return this.mGuestRepository.getGuestsByQuery("select * from " + DatabaseConstants.GUEST.TABLE_NAME);
    }

    public List<Guest> getPresent(){
        return this.mGuestRepository.getGuestsByQuery("select * from " + DatabaseConstants.GUEST.TABLE_NAME + " where " +
        DatabaseConstants.GUEST.COLUMNS.PRESENCE + " = " + GuestConstants.CONFIRMATION.PRESENT);
    }

    public List<Guest> getAbsent(){
        return this.mGuestRepository.getGuestsByQuery("select * from " + DatabaseConstants.GUEST.TABLE_NAME + " where " +
                DatabaseConstants.GUEST.COLUMNS.PRESENCE + " = " + GuestConstants.CONFIRMATION.ABSENT);
    }

    public Guest load(int mGuestId) {
        return this.mGuestRepository.load(mGuestId);
    }

    public boolean update(Guest guest) {
        return this.mGuestRepository.update(guest);
    }

    public boolean remove(int id) {
        return this.mGuestRepository.remove(id);
    }

    public GuestCount loadDashboard() {

        return this.mGuestRepository.loadDashboard();
    }
}
