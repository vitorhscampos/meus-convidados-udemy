package com.example.indb.meusconvidados.models;

public class GuestCount {

    private int presentCount;
    private int absentCount;
    private int allCount;

    public GuestCount(int present, int absent, int all){
        this.presentCount = present;
        this.absentCount = absent;
        this.allCount = all;
    }

    public int getPresentCount() {
        return presentCount;
    }

    public void setPresentCount(int presentCount) {
        this.presentCount = presentCount;
    }

    public int getAbsentCount() {
        return absentCount;
    }

    public void setAbsentCount(int absentCount) {
        this.absentCount = absentCount;
    }

    public int getAllCount() {
        return allCount;
    }

    public void setAllCount(int allCount) {
        this.allCount = allCount;
    }
}
