package com.example.indb.meusconvidados.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.indb.meusconvidados.R;
import com.example.indb.meusconvidados.activities.GuestFormActivity;
import com.example.indb.meusconvidados.adapters.GuestListAdapter;
import com.example.indb.meusconvidados.business.GuestBusiness;
import com.example.indb.meusconvidados.constants.GuestConstants;
import com.example.indb.meusconvidados.models.Guest;
import com.example.indb.meusconvidados.listeners.OnGuestListInteractionListener;
import com.example.indb.meusconvidados.models.GuestCount;

import java.util.List;

public class AbsentFragment extends Fragment {

    private OnGuestListInteractionListener listener;

    private static class ViewHolder{
        RecyclerView mRecyclerAbsent;
        TextView mPresentCount;
        TextView mAbsentCount;
        TextView mTotalCount;
    }

    private ViewHolder mViewHolder = new ViewHolder();
    private GuestBusiness mGuestBusiness;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_absent, container, false);
        Context context = view.getContext();

        this.mViewHolder.mRecyclerAbsent = view.findViewById(R.id.recycler_absent);
        this.mViewHolder.mPresentCount = view.findViewById(R.id.text_present_count);
        this.mViewHolder.mAbsentCount = view.findViewById(R.id.text_absent_count);
        this.mViewHolder.mTotalCount = view.findViewById(R.id.text_all_count);

        this.mGuestBusiness = new GuestBusiness(context);

        this.listener = new OnGuestListInteractionListener() {
            @Override
            public void onListClick(int id) {
                Bundle bundle = new Bundle();
                bundle.putInt(GuestConstants.BundleConstants.GUEST_ID, id);

                Intent intent = new Intent(getContext(), GuestFormActivity.class);
                intent.putExtras(bundle);

                startActivity(intent);

            }

            @Override
            public void onDeleteClick(int id) {
                mGuestBusiness.remove(id);
                Toast.makeText(getContext(), R.string.guest_removed, Toast.LENGTH_SHORT).show();
                loadDashboard();
                loadGuests();
            }
        };

        //Definir um layout
        this.mViewHolder.mRecyclerAbsent.setLayoutManager(new LinearLayoutManager(context));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        this.loadDashboard();
        this.loadGuests();

    }

    private void loadGuests() {
        List<Guest> guestList =  this.mGuestBusiness.getAbsent();

        GuestListAdapter guestListAdapter = new GuestListAdapter(guestList, listener);
        this.mViewHolder.mRecyclerAbsent.setAdapter(guestListAdapter);
        guestListAdapter.notifyDataSetChanged();
    }

    private void loadDashboard() {
        GuestCount guestCount = this.mGuestBusiness.loadDashboard();

        this.mViewHolder.mPresentCount.setText(String.valueOf(guestCount.getPresentCount()));
        this.mViewHolder.mAbsentCount.setText(String.valueOf(guestCount.getAbsentCount()));
        this.mViewHolder.mTotalCount.setText(String.valueOf(guestCount.getAllCount()));
    }
}
